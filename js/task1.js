"use strict";
/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
 При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
 При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

console.log("THEORY");

console.log("1. Він потрібен для відміни стандартної дії браузера, яка мала відбутися за замовчуванням, наприклад, перехід за лінком після натискання на елемент, створений з тегом a ");
console.log("2. В тому, щоб не навішувати eventListener на кожний елемент з великого списку (наприклад, можна клікнути одразу на багато карток чи зображень). Замість цього, eventListener додається до спільного батька цих елементів, і замість сотень або тисяч івентів ми отримуємо один, який буде задіяний при кліку на будь-який дочірній елемент. клікнутий дочірній елемент ми визначаємо за допомогою event.target");
console.log("3. події буферу обміну, клік та переміщення мишки, натискання клавіш клавіатури, прогрузка DOM. готовність браузера до відображення сторінки, якісь події зі сторони сервера");



console.log("PRACTICE");

let textList = document.querySelectorAll(`.tabs-content>li`);

textList.forEach((elem) => {
    elem.style.display = "none";
    textList[0].id = "activated";
    return textList[0].style.display = "block";    //Даю всім текстам новий клас і паралельно, даю першому тексту Айді і залишаю його на сторінці, інші тексти прибираю
});


const menuList = document.querySelector(".tabs");  //дав імя списку меню

menuList.addEventListener("click", (event) => {
    let activeButton = document.querySelector(".tabs-title.active"); //змінна, яка відповідає за активний елемент меню на початку. далі заміняється на target
    const target = event.target.closest('.tabs-title');

    activeButton.classList.remove("active");
    target.classList.add('active');
    activeButton = target;
    console.log(activeButton.textContent.toLowerCase());


    let menuItemClicked = activeButton.textContent.toLowerCase();

    textList.forEach((elem) => {    //роблю перебір списку текстів ще раз, якщо дата-атрибут співпадає з назвою тексту - цей текст вставляється під час кліку на елемент меню
        elem.style.display = "none";
        if (elem.dataset.textBelong === menuItemClicked) {
            elem.style.display = "block";
        }
    })





    //  if (activeText.dataset.textBelong === menuItemClicked) {

    //  }


    // if (activeButton.textContent === "Akali") {
    //     textList[0].id = "activated";
    //     textList[0].style.display = "block"
    // }

});

/* window.addEventListener('keydown', (event) => {
    console.log(first);
    const activeElem = document.querySelector('.active');

    if (activeElem) {
        activeElem.classList.remove('active');
    }

    el = document.querySelector(`#${event.code.toLowerCase()}-counter`);

    if (el) {
        el.innerText = +el.innerText + 1;
        el.classList.add('active');
    }
}) */



/* const productElements = document.querySelectorAll('.store>li');
console.log(productElements)

productElements.forEach((elem) => { */


